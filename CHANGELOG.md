## 1.2.0 (2022-07-25)

### Feature

- [Feature B](milhamdedi/pocs@35487da9a2aa4eda7e64722c0acb27d1bd5fb7bd)

### Fix

- [Fix on Bug Y](milhamdedi/pocs@cef4ccc27fca962d6c124967738e46797b9c03ba)

### other

- [More Categories for Changelog](milhamdedi/pocs@1610ef1ced66c447dc0b6791d1afc7be8726020e)

## 1.1.0 (2022-07-22)

### Feature

- [Feature B](milhamdedi/pocs@35487da9a2aa4eda7e64722c0acb27d1bd5fb7bd)

### Fix

- [Fix on Bug Y](milhamdedi/pocs@cef4ccc27fca962d6c124967738e46797b9c03ba)

### other

- [More Categories for Changelog](milhamdedi/pocs@1610ef1ced66c447dc0b6791d1afc7be8726020e)

## 1.0.0 (2022-07-22)

No changes.
